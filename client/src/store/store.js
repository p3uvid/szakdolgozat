import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,

  plugins: [
    createPersistedState()
  ],

  state: {
    token: null,
    user: null,
    userTeams: null,
    isUserLoggedIn: false
  },

  mutations: {
    setToken (state, token) {
      state.token = token
      state.isUserLoggedIn = !!(token)
    },

    setUser (state, user) {
      state.user = user
    },

    setUserTeams (state, teams) {
      state.userTeams = teams
    }
  },

  actions: {
    setToken ({commit}, token) {
      commit('setToken', token)
    },

    setUser ({commit}, user) {
      commit('setUser', user)
    },

    setUserTeams ({commit}, teams) {
      commit('setUserTeams', teams)
    }
  }
})
