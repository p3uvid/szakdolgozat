import { Bar } from 'vue-chartjs'

export default {
  extends: Bar,
  props: ['datalabel', 'color', 'labels', 'datas'],
  methods: {
    render () {
      // Overwriting base render method with actual data.
      this.renderChart({
        labels: this.labels,
        datasets: [
          {
            label: this.datalabel,
            backgroundColor: this.color,
            data: this.datas
          }
        ]
      })
    }
  },
  mounted () {
    this.render()
  },
  watch: {
    datas: function () {
      this.render()
    }
  }
}
