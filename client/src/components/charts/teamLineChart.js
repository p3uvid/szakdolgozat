import { Line } from 'vue-chartjs'

export default {
  extends: Line,
  props: ['labels', 'datasets'],
  methods: {
    render () {
      // Overwriting base render method with actual data.
      this.renderChart({
        labels: this.labels,
        datasets: this.datasets
      })
    }
  },
  mounted () {
    this.render()
  },
  watch: {
    datasets: function () {
      this.render()
    }
  }
}
