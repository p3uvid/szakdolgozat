// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
// import { sync } from 'vuex-router-sync'
import store from '@/store/store'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

import VueSidebarMenu from 'vue-sidebar-menu'
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'
import FullCalendar from 'vue-full-calendar'
import 'fullcalendar/dist/fullcalendar.css'

import Datetime from 'vue-datetime'
import 'vue-datetime/dist/vue-datetime.css'

import Multiselect from 'vue-multiselect'
import 'vue-multiselect/dist/vue-multiselect.min.css'

import VueCharts from 'vue-chartjs'

Vue.use(VueSidebarMenu)
Vue.use(FullCalendar)
Vue.use(Datetime)
Vue.component('multiselect', Multiselect)
Vue.use(VueCharts)

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.config.productionTip = false

// sync(store, router) // causes error: vuex route state overridden route module

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
