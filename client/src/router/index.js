import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/page-components/Index'
import Login from '@/components/page-components/Login'
import Settings from '@/components/page-components/Settings'
import Orders from '@/components/page-components/Orders'
import Materials from '@/components/page-components/Materials'
import Surveys from '@/components/page-components/Surveys'
import Repairs from '@/components/page-components/Repairs'
import Calendar from '@/components/page-components/Calendar'
import Statistics from '@/components/page-components/Statistics'
import Profile from '@/components/page-components/Profile'
import Notes from '@/components/page-components/Notes'
import store from '@/store/store.js'

Vue.use(Router)

const ifNotAuthenticated = (to, from, next) => {
  if (!store.state.isUserLoggedIn) {
    next()
    return
  }
  next('/')
}

const ifAuthenticated = (to, from, next) => {
  if (store.state.isUserLoggedIn) {
    next()
    return
  }
  next('/login')
}

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      beforeEnter: ifNotAuthenticated
    },
    {
      path: '/settings',
      name: 'settings',
      component: Settings,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/orders',
      name: 'orders',
      component: Orders,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/materials',
      name: 'materials',
      component: Materials,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/works/surveys',
      name: 'surveys',
      component: Surveys,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/works/repairs',
      name: 'repairs',
      component: Repairs,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/calendar',
      name: 'calendar',
      component: Calendar,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/statistics',
      name: 'statistics',
      component: Statistics,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/notes',
      name: 'notes',
      component: Notes,
      beforeEnter: ifAuthenticated
    }
  ]
})
