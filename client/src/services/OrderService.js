import Api from '@/services/Api'

export default {
  getOrders () {
    return Api().get('orders')
  },

  getOrder (params) {
    return Api().get('orders/' + params.id)
  },

  addOrder (params) {
    return Api().post('orders/add/', params)
  },

  addWorkToOrder (params) {
    return Api().put('orders/addwork/' + params.id, params)
  },

  deleteWorkFromOrder (params) {
    return Api().put('orders/deletework/' + params.id, params)
  },

  updateOrder (params) {
    return Api().put('orders/' + params.id, params)
  },

  deleteOrder (id) {
    return Api().delete('orders/' + id)
  }
}
