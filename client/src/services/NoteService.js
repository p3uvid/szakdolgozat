import Api from '@/services/Api'

export default {
  getNotes () {
    return Api().get('notes')
  },

  getNote (params) {
    return Api().get('notes/' + params.id)
  },

  addNote (params) {
    return Api().post('notes/add/', params)
  },

  deleteNote (id) {
    return Api().delete('notes/' + id)
  },

  getComments (params) {
    return Api().get('notes/' + params.id + '/comments')
  },

  addComment (params) {
    return Api().post('notes/' + params.note + '/addcomment', params)
  }
}
