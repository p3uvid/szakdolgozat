import Api from '@/services/Api'

export default {
  getTeams () {
    return Api().get('teams')
  },

  getTeam (params) {
    return Api().get('teams/' + params.id)
  },

  addTeam (params) {
    return Api().post('teams/add/', params)
  },

  updateTeam (params) {
    return Api().put('teams/' + params.id, params)
  },

  assignUserToTeam (params) {
    return Api().put('teams/adduser/' + params.id, params)
  },

  deleteUserFromTeam (params) {
    return Api().put('teams/deleteuser/' + params.id, params)
  },

  deleteTeam (id) {
    return Api().delete('teams/' + id)
  }
}
