import Api from '@/services/Api'

export default {
  getUsers () {
    return Api().get('users')
  },

  getUser (params) {
    return Api().get('users/' + params.id)
  },

  addUser (params) {
    return Api().post('users/', params)
  },

  updateUser (params) {
    return Api().put('users/' + params.id, params)
  },

  updatePassword (params) {
    return Api().put('users/password/' + params.id, params)
  },

  restorePassword (params) {
    return Api().put('users/restorePassword/' + params.id, params)
  },

  deleteUser (id) {
    return Api().delete('users/' + id)
  }
}
