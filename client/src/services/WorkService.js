import Api from '@/services/Api'

export default {
  getWorks () {
    return Api().get('works')
  },

  getSurveys () {
    return Api().get('works/surveys')
  },

  getRepairs () {
    return Api().get('works/repairs')
  },

  getWork (params) {
    return Api().get('works/' + params.id)
  },

  addWork (params) {
    return Api().post('works/add/', params)
  },

  updateWork (params) {
    return Api().put('works/' + params.id, params)
  },

  deleteWork (id) {
    return Api().delete('works/' + id)
  }
}
