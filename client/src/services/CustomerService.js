import Api from '@/services/Api'

export default {
  getCustomers () {
    return Api().get('customers')
  },

  getCustomer (params) {
    return Api().get('customers/' + params.id)
  },

  addCustomer (params) {
    return Api().post('customers/add/', params)
  },

  updateCustomer (params) {
    return Api().put('customers/' + params.id, params)
  },

  deleteCustomer (id) {
    return Api().delete('customers/' + id)
  }
}
