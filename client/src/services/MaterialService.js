import Api from '@/services/Api'

export default {
  getMaterials () {
    return Api().get('materials')
  },

  getMaterial (params) {
    return Api().get('materials/' + params.id)
  },

  addMaterial (params) {
    return Api().post('materials/add', params)
  },

  updateMaterialStatus (params) {
    return Api().put('materials/status/' + params.id, params)
  },

  updateMaterial (params) {
    return Api().put('materials/' + params.id, params)
  },

  deleteMaterial (id) {
    return Api().delete('materials/' + id)
  }
}
