const dbHandler = require('./db-handler')
const MaterialModel = require('../models/materials')

let firstMaterialId = null

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => await dbHandler.connect())

/**
 * Remove and close the db and server.
 */
afterAll(async () => await dbHandler.closeDatabase())

/**
 * Material test suite.
 */
describe('Material model ', () => {
  it('create & save material successfully', async () => {
    const material = new MaterialModel(validMaterial)
    const savedMaterial = await material.save()
    // Object Id should be defined when successfully saved to MongoDB.
    expect(savedMaterial._id).toBeDefined()
    expect(savedMaterial.name).toBe(validMaterial.name)
    expect(savedMaterial.price).toBe(validMaterial.price)
    expect(savedMaterial.number).toBe(validMaterial.number)
    expect(savedMaterial.status).toBe(validMaterial.status)

    firstMaterialId = savedMaterial._id // use for get material by id

    // Add two another valid material for future testing
    await new MaterialModel(validMaterial2).save()
    await new MaterialModel(validMaterial3).save()
  })

  // Insert a material with a status that is not definied in the schema
  it('create material with invalid status type', async () => {
    let error = null
    try {
      const materialWithInvalidStatus = new MaterialModel(invalidMaterial)
      await materialWithInvalidStatus.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull() // validation error
  })

  // Get material (first material) by id
  it('get material by id', async () => {
    const material = await MaterialModel.findById(firstMaterialId)
    expect(material._id).toBeDefined()
    expect(material.name).toBe(validMaterial.name)
    expect(material.price).toBe(validMaterial.price)
    expect(material.number).toBe(validMaterial.number)
    expect(material.status).toBe(validMaterial.status)
  })

  // Get all materials
  it('get all materials from db', async () => {
    const materials = await MaterialModel.find()
    expect(materials).toHaveLength(3) // three inserted material
    expect(materials[0].name).toBe(validMaterial.name)
  })

  // Modify first material data
  it('modify material data', async () => {
    const material = await MaterialModel.findById(firstMaterialId)
    material.name = modifiedMaterial.name
    material.price = modifiedMaterial.price
    material.number = modifiedMaterial.number
    material.status = modifiedMaterial.status
    const savedMaterial = await material.save()

    expect(savedMaterial.name).toBe(modifiedMaterial.name)
    expect(savedMaterial.price).toBe(modifiedMaterial.price)
    expect(savedMaterial.number).toBe(modifiedMaterial.number)
    expect(savedMaterial.status).toBe(modifiedMaterial.status)
  })

  // Delete material
  it('delete material', async () => {
    await MaterialModel.deleteOne({ _id: firstMaterialId })
    const material = await MaterialModel.findById(firstMaterialId)
    const materials = await MaterialModel.find()
    expect(material).toBe(null)
    expect(materials).toHaveLength(2)
  })
})

/**
 * Complete material model examples.
 */
const validMaterial = {
  name: 'Test material name',
  price: 1500,
  number: 20,
  status: 'inStock'
}

const validMaterial2 = {
  name: 'Test material name 2',
  price: 1500,
  number: 20,
  status: 'few'
}

const validMaterial3 = {
  name: 'Test material name 3',
  price: 1500,
  number: 20,
  status: 'order'
}

const invalidMaterial = {
  name: 'Invalid material',
  price: 1500,
  number: 20,
  status: 'invalid'
}

const modifiedMaterial = {
  name: 'Modified material',
  price: 1000,
  number: 10,
  status: 'few'
}