const dbHandler = require('./db-handler')
const CustomerModel = require('../models/customers')

const EMAIL_REGEX = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g

let firstCustomerId = null

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => await dbHandler.connect())

/**
 * Remove and close the db and server.
 */
afterAll(async () => await dbHandler.closeDatabase())

/**
 * Customer test suite.
 */
describe('Customer model ', () => {
  it('create & save customer successfully', async () => {
    const customer = new CustomerModel(validCustomer)
    const savedCustomer = await customer.save()
    // Object Id should be defined when successfully saved to MongoDB.
    expect(savedCustomer._id).toBeDefined()
    expect(savedCustomer.name).toBe(validCustomer.name)
    expect(savedCustomer.address).toBe(validCustomer.address)
    expect(savedCustomer.email).toBe(validCustomer.email)
    expect(savedCustomer.email).toMatch(EMAIL_REGEX)
    expect(savedCustomer.phone).toBe(validCustomer.phone)

    firstCustomerId = savedCustomer._id // use for get customer by id
  })

  // Email field is unique, error is expected
  it('create customer with existing email address, error should be not null', async () => {
    let error = null
    try {
      const customer = new CustomerModel(validCustomer)
      await customer.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull() // mongo error
  })

  // Get customer (first customer) by id
  it('get customer by id', async () => {
    const customer = await CustomerModel.findById(firstCustomerId)
    expect(customer._id).toBeDefined()
    expect(customer.name).toBe(validCustomer.name)
    expect(customer.address).toBe(validCustomer.address)
    expect(customer.email).toBe(validCustomer.email)
    expect(customer.email).toMatch(EMAIL_REGEX)
    expect(customer.phone).toBe(validCustomer.phone)
  })

  // Get all customers
  it('get all customers from db', async () => {
    const customers = await CustomerModel.find()
    expect(customers).toHaveLength(1) // one inserted customer
    expect(customers[0].email).toBe(validCustomer.email)
  })

  // Modify first customer data
  it('modify customer data', async () => {
    const customer = await CustomerModel.findById(firstCustomerId)
    customer.name = modifiedCustomer.name
    customer.address = modifiedCustomer.address
    customer.email = modifiedCustomer.email
    customer.phone = modifiedCustomer.phone
    const savedCustomer = await customer.save()

    expect(savedCustomer.name).toBe(modifiedCustomer.name)
    expect(savedCustomer.address).toBe(modifiedCustomer.address)
    expect(savedCustomer.email).toBe(modifiedCustomer.email)
    expect(savedCustomer.email).toMatch(EMAIL_REGEX)
    expect(savedCustomer.phone).toBe(modifiedCustomer.phone)
  })

  // Delete customer
  it('delete customer', async () => {
    await CustomerModel.deleteOne({ _id: firstCustomerId })
    const customer = await CustomerModel.findById(firstCustomerId)
    const customers = await CustomerModel.find()
    expect(customer).toBe(null)
    expect(customers).toHaveLength(0)
  })
})

/**
 * Complete customer model examples.
 */
const validCustomer = {
  name: 'Test customer name',
  address: 'Test address',
  email: 'testcustomer@test.com',
  phone: '06201234567'
}

const modifiedCustomer = {
  name: 'Modified Test customer name',
  address: 'Modified Test address',
  email: 'testcustomermodified@test.com',
  phone: '06201112222'
}