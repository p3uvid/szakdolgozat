const mongoose = require('mongoose')

const dbHandler = require('./db-handler')
const NoteModel = require('../models/notes')
const Note = require('../models/notes')

let firstNoteId = null

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => await dbHandler.connect())

/**
 * Remove and close the db and server.
 */
afterAll(async () => await dbHandler.closeDatabase())

/**
 * Note test suite.
 */
describe('Note model ', () => {
  it('create & save note successfully', async () => {
    const note = new NoteModel(validNote)
    const savedNote = await note.save()

    const user = mongoose.Types.ObjectId(savedNote.user).toHexString()
    expect(savedNote._id).toBeDefined()
    expect(typeof savedNote.user).toBe('object')
    expect(user).toEqual(validNote.user)
    expect(savedNote.title).toBe(validNote.title)
    expect(savedNote.text).toBe(validNote.text)
    expect(savedNote.date).toBe(validNote.date)

    firstNoteId = savedNote._id // use for get note by id

    // Add two another valid note for future testing
    await new NoteModel(validNote).save()
    await new NoteModel(validNote).save()
  })

  // Note with invalid user id causes error
  it('create note with invalid user id causes error', async () => {
    let error = null
    try {
      const noteWithInvalidUser = new NoteModel(invalidNote)
      await noteWithInvalidUser.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull() // validation error
  })

  // Note without reqiured fields causes error
  it('create note without required fields causes error', async () => {
    let error = null
    try {
      await new NoteModel({}).save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull() // validation error
  })

  // Get note (first note) by id
  it('get note by id', async () => {
    const note = await NoteModel.findById(firstNoteId)

    const user = mongoose.Types.ObjectId(note.user).toHexString()
    expect(note._id).toBeDefined()
    expect(typeof note.user).toBe('object')
    expect(user).toEqual(validNote.user)
    expect(note.title).toBe(validNote.title)
    expect(note.text).toBe(validNote.text)
    expect(note.date).toStrictEqual(validNote.date)
  })

  // Get all notes
  it('get all notes from db', async () => {
    const notes = await NoteModel.find()
    expect(notes).toHaveLength(3) // three inserted notes
    expect(notes[0].text).toBe(validNote.text)
    expect(notes[1].text).toBe(validNote.text)
    expect(notes[2].text).toBe(validNote.text)
  })

  // Modify first note data
  it('modify note data', async () => {
    const note = await NoteModel.findById(firstNoteId)
    note.title = modifiedNote.title
    note.text = modifiedNote.text
    note.date = modifiedNote.date
    const savedNote = await note.save()

    const user = mongoose.Types.ObjectId(savedNote.user).toHexString()
    const noteUser = mongoose.Types.ObjectId(note.user).toHexString()

    expect(user).toBe(noteUser)
    expect(savedNote.title).toBe(modifiedNote.title)
    expect(savedNote.text).toBe(modifiedNote.text)
    expect(savedNote.date).toStrictEqual(modifiedNote.date)
  })

  // Delete note
  it('delete note', async () => {
    await NoteModel.deleteOne({ _id: firstNoteId })
    const note = await NoteModel.findById(firstNoteId)
    const notes = await NoteModel.find()
    expect(note).toBe(null)
    expect(notes).toHaveLength(2)
  })
})

/**
 * Complete note model examples.
 */
const validNote = {
  user: '5fbe471792cf6928e43c8fb4',
  title: 'Test title',
  text: 'Test text',
  date: new Date('2020-10-13T13:45:52.000+00:00')
}

const invalidNote = {
  user: '1',
  title: 'Test title',
  text: 'Test text',
  date: new Date()
}

const modifiedNote = {
  title: 'Test title modified',
  text: 'Test text modified',
  date: new Date()
}