const mongoose = require('mongoose')

const dbHandler = require('./db-handler')
const TeamModel = require('../models/teams')
const Team = require('../models/teams')

let firstTeamId = null

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => await dbHandler.connect())

/**
 * Remove and close the db and server.
 */
afterAll(async () => await dbHandler.closeDatabase())

/**
 * Team test suite.
 */
describe('Team model ', () => {
  it('create & save team successfully', async () => {
    const team = new TeamModel(validTeam)
    const savedTeam = await team.save()

    expect(savedTeam._id).toBeDefined()
    expect(savedTeam.name).toBe(validTeam.name)
    expect(savedTeam.members).toHaveLength(validTeam.members.length)

    firstTeamId = savedTeam._id // use for get team by id
  })

  it('create & save team with member successfully', async () => {
    const team = new TeamModel(validTeamWithMember)
    const savedTeam = await team.save()

    expect(savedTeam._id).toBeDefined()
    expect(savedTeam.name).toBe(validTeamWithMember.name)
    expect(savedTeam.members).toHaveLength(validTeamWithMember.members.length)
  })

  // Add member to a team
  it('add (and delete) member to the first team', async () => {
    const team = await TeamModel.findById(firstTeamId)
    expect(team.members).toHaveLength(0)

    team.members.push('5fbe616104cba8484003bc57')
    let savedTeam = await team.save()
    expect(savedTeam.members).toHaveLength(1)
    expect(typeof savedTeam.members[0]).toBe('object')

    savedTeam.members.splice(0, 1)
    savedTeam = await savedTeam.save()
    expect(savedTeam.members).toHaveLength(0)
  })

  // Get team (first team) by id
  it('get order by id', async () => {
    const team = await TeamModel.findById(firstTeamId)

    expect(team.name).toBe(validTeam.name)
    expect(team.members).toHaveLength(validTeam.members.length)
  })

  // Get all teams
  it('get all teams from db', async () => {
    const teams = await TeamModel.find()
    expect(teams).toHaveLength(2) // two inserted team
    expect(teams[0].members).toHaveLength(validTeam.members.length)
    expect(teams[1].members).toHaveLength(validTeamWithMember.members.length)
  })

  // Modify first team data
  it('modify team data', async () => {
    const team = await TeamModel.findById(firstTeamId)
    team.name = modifiedTeam.name
    team.members = modifiedTeam.members
    const savedTeam = await team.save()

    expect(savedTeam.name).toBe(modifiedTeam.name)
    expect(savedTeam.members).toHaveLength(modifiedTeam.members.length)
  })

  // Delete team
  it('delete team', async () => {
    await TeamModel.deleteOne({ _id: firstTeamId })
    const team = await TeamModel.findById(firstTeamId)
    const teams = await TeamModel.find()
    expect(team).toBe(null)
    expect(teams).toHaveLength(1)
  })
})

/**
 * Complete team model examples.
 */
const validTeam = {
  name: 'First team',
  members: []
}

const validTeamWithMember = {
  name: 'Second team',
  members: ['5fbe54f2ede3534a0893a205']
}

const modifiedTeam = {
  name: 'Modified name',
  members: ['5fbe616104cba8484003bc57']
}