const mongoose = require('mongoose')

const dbHandler = require('./db-handler')
const OrderModel = require('../models/orders')

let firstOrderId = null

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => await dbHandler.connect())

/**
 * Remove and close the db and server.
 */
afterAll(async () => await dbHandler.closeDatabase())

/**
 * Order test suite.
 */
describe('Order model ', () => {
  it('create & save order successfully', async () => {
    const order = new OrderModel(validOrder)
    const savedOrder = await order.save()

    const customer = mongoose.Types.ObjectId(savedOrder.customer).toHexString()
    // Object Id should be defined when successfully saved to MongoDB.
    expect(savedOrder._id).toBeDefined()
    expect(savedOrder.status).toBe(validOrder.status)
    expect(savedOrder.works).toHaveLength(validOrder.works.length)
    expect(typeof savedOrder.customer).toBe('object')
    expect(customer).toBe(validOrder.customer)

    firstOrderId = savedOrder._id // use for get order by id

    // Add three another valid order (with valid status) for future testing
    await new OrderModel(validOrder2).save()
    await new OrderModel(validOrder3).save()
    await new OrderModel(validOrder4).save()
  })

  // Add work to an order
  it('add and delete work to the first order', async () => {
    const order = await OrderModel.findById(firstOrderId)
    expect(order.works).toHaveLength(0)

    order.works.push('5fbe616104cba8484003bc57')
    let savedOrder = await order.save()
    expect(savedOrder.works).toHaveLength(1)
    expect(typeof savedOrder.works[0]).toBe('object')

    savedOrder.works.splice(0, 1)
    savedOrder = await savedOrder.save()
    expect(savedOrder.works).toHaveLength(0)
  })

  // Insert an order with a status that is not definied in the schema
  it('create order with invalid status type', async () => {
    let error = null
    try {
      await new OrderModel(invalidOrder).save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull() // validation error
  })

  // Get order (first order) by id
  it('get order by id', async () => {
    const order = await OrderModel.findById(firstOrderId)
    const customer = mongoose.Types.ObjectId(order.customer).toHexString()

    expect(order.status).toBe(validOrder.status)
    expect(order.works).toHaveLength(validOrder.works.length)
    expect(typeof order.customer).toBe('object')
    expect(customer).toBe(validOrder.customer)
  })

  // Get all orders
  it('get all orders from db', async () => {
    const orders = await OrderModel.find()
    const customer = mongoose.Types.ObjectId(orders[0].customer).toHexString()
    expect(orders).toHaveLength(4) // four inserted order
    expect(customer).toBe(validOrder.customer)
  })

  // Modify first order data
  it('modify order data', async () => {
    const order = await OrderModel.findById(firstOrderId)
    order.status = modifiedOrder.status
    order.works = modifiedOrder.works
    const savedOrder = await order.save()

    const customer = mongoose.Types.ObjectId(savedOrder.customer).toHexString()
    const orderCustomer = mongoose.Types.ObjectId(order.customer).toHexString()
    expect(savedOrder.status).toBe(modifiedOrder.status)
    expect(savedOrder.works).toHaveLength(modifiedOrder.works.length)
    expect(typeof savedOrder.customer).toBe('object')
    expect(customer).toBe(orderCustomer)
  })

  // Delete order
  it('delete order', async () => {
    await OrderModel.deleteOne({ _id: firstOrderId })
    const order = await OrderModel.findById(firstOrderId)
    const orders = await OrderModel.find()
    expect(order).toBe(null)
    expect(orders).toHaveLength(3)
  })
})

/**
 * Complete order model examples.
 */
const validOrder = {
  customer: '5fbe54f2ede3534a0893a205',
  status: 'survey',
  works: []
}

const validOrder2 = {
  customer: '5fbe54f2ede3534a0893a205',
  status: 'waitingForRepair',
  works: []
}

const validOrder3 = {
  customer: '5fbe54f2ede3534a0893a205',
  status: 'repair',
  works: []
}

const validOrder4 = {
  customer: '5fbe54f2ede3534a0893a205',
  status: 'done',
  works: []
}

const invalidOrder = {
  customer: '5fbe54f2ede3534a0893a205',
  status: 'cool',
  works: []
}

const modifiedOrder = {
  status: 'done',
  works: ['5fbe616104cba8484003bc57']
}