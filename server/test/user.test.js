const dbHandler = require('./db-handler')
const UserModel = require('../models/users')
const User = require('../models/users')

const HASH_REGEX = /^\$2[ayb]\$.{56}$/g
const EMAIL_REGEX = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g

let firstUserId = null

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => await dbHandler.connect())

/**
 * Remove and close the db and server.
 */
afterAll(async () => await dbHandler.closeDatabase())

/**
 * User test suite.
 */
describe('User model ', () => {
  it('create & save user successfully', async () => {
    const user = new UserModel(validUser)
    const savedUser = await user.save()
    // Object Id should be defined when successfully saved to MongoDB.
    expect(savedUser._id).toBeDefined()
    expect(savedUser.name).toBe(validUser.name)
    expect(savedUser.username).toBe(validUser.username)
    expect(savedUser.email).toBe(validUser.email)
    expect(savedUser.email).toMatch(EMAIL_REGEX)
    expect(savedUser.role).toBe(validUser.role)
    expect(savedUser.password).toMatch(HASH_REGEX)

    firstUserId = savedUser._id // use for get user by id
  })

  // You shouldn't be able to add in any field that isn't defined in the schema
  it('insert user successfully, but the field does not defined in schema should be undefined', async () => {
    const userWithInvalidField = new UserModel(validUser2)
    const savedUserWithInvalidField = await userWithInvalidField.save()
    expect(savedUserWithInvalidField._id).toBeDefined()
    expect(savedUserWithInvalidField.nickkname).toBeUndefined()
  })

  // Email field is unique, error is expected
  it('create user with existing email address, error should be not null', async () => {
    let error = null
    try {
      await new UserModel(validUser).save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull() // mongo error
  })

  // Insert a user with a role that is not definied in the schema
  it('create user with invalid role type', async () => {
    let error = null
    try {
      const userWithInvalidRole = new UserModel(invalidRoleUser)
      await userWithInvalidRole.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull() // validation error
  })

  // Get user (first user) by id
  it('get user by id', async () => {
    const user = await UserModel.findById(firstUserId)
    expect(user._id).toBeDefined()
    expect(user.name).toBe(validUser.name)
    expect(user.username).toBe(validUser.username)
    expect(user.email).toBe(validUser.email)
    expect(user.role).toBe(validUser.role)
    expect(user.password).toMatch(HASH_REGEX)
  })

  // Get all users
  it('get all users from db', async () => {
    const users = await UserModel.find()
    expect(users).toHaveLength(2) // two inserted user
    expect(users[0].email).toBe(validUser.email)
    expect(users[1].email).toBe(validUser2.email)
  })

  // Modify first user data
  it('modify user data (without password)', async () => {
    const user = await UserModel.findById(firstUserId)
    user.name = modifiedUser.name
    user.email = modifiedUser.email
    user.username = modifiedUser.username
    user.role = modifiedUser.role
    const savedUser = await user.save()
    expect(savedUser.name).toBe(modifiedUser.name)
    expect(savedUser.email).toBe(modifiedUser.email)
    expect(savedUser.username).toBe(modifiedUser.username)
    expect(savedUser.role).toBe(modifiedUser.role)
    expect(savedUser.password).toMatch(HASH_REGEX)
  })

  // Modify first user's password
  it('modify user password', async () => {
    const user = await UserModel.findById(firstUserId)
    user.password = 'newpassword'
    const savedUser = await user.save()
    expect(savedUser.email).toBe(user.email)
    expect(savedUser.password).not.toBe('newpassword')
    expect(savedUser.password).toMatch(HASH_REGEX)
  })

  // Delete user
  it('delete user', async () => {
    await UserModel.deleteOne({ _id: firstUserId })
    const user = await UserModel.findById(firstUserId)
    const users = await UserModel.find()
    expect(user).toBe(null)
    expect(users).toHaveLength(1)
  })
})

/**
 * Complete user model examples.
 */
const validUser = {
    name: 'Test user name',
    email: 'test@test.com',
    username: 'testuser',
    password: 'test',
    role: 'user'
}

const validUser2 = {
  name: 'Test user name 2',
  email: 'test2@test.com',
  username: 'testuser2',
  password: 'test',
  role: 'admin'
}

const invalidRoleUser = {
  name: 'Test user 3',
  email: 'test3@test.com',
  username: 'testuser3',
  password: 'test',
  role: 'invalid-user'
}

const modifiedUser = {
  name: 'Modified Test user name',
  email: 'tesmodified@test.com',
  username: 'modifieduser',
  role: 'teamleader'
}