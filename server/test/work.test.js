const mongoose = require('mongoose')

const dbHandler = require('./db-handler')
const WorkModel = require('../models/works')

let firstWorkId = null

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => await dbHandler.connect())

/**
 * Remove and close the db and server.
 */
afterAll(async () => await dbHandler.closeDatabase())

/**
 * Work test suite.
 */
describe('Work model ', () => {
  it('create & save work successfully', async () => {
    const work = new WorkModel(validWork)
    const savedWork = await work.save()

    const customer = mongoose.Types.ObjectId(savedWork.customer).toHexString()
    const team = mongoose.Types.ObjectId(savedWork.team).toHexString()
    // Object Id should be defined when successfully saved to MongoDB.
    expect(savedWork._id).toBeDefined()
    expect(savedWork.type).toBe(validWork.type)
    expect(savedWork.done).toBe(validWork.done)
    expect(savedWork.duration).toBe(validWork.duration)
    expect(typeof savedWork.customer).toBe('object')
    expect(customer).toBe(validWork.customer)
    expect(savedWork.date).toBe(validWork.date)
    expect(savedWork.endDate).toBe(validWork.endDate)
    expect(savedWork.cost).toBe(validWork.cost)
    expect(savedWork.problem).toBe(validWork.problem)
    expect(typeof savedWork.team).toBe('object')
    expect(team).toBe(validWork.team)
    expect(savedWork.materials).toHaveLength(validWork.materials.length)

    firstWorkId = savedWork._id // use for get work by id

    // Insert another valid work
    await new WorkModel(validWorkReapir).save()
  })

  // Work with invalid work type causes error
  it('create work with invalid work type causes error', async () => {
    let error = null
    try {
      const workWithInvalidType = new WorkModel(invalidWorkType)
      await workWithInvalidType.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull() // validation error
  })

  // Work with invalid cost causes error
  it('create work with invalid cost value causes error', async () => {
    let error = null
    try {
      const workWithInvalidCost = new WorkModel(invalidWorkCost)
      await workWithInvalidCost.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull() // validation error
  })

  // Work with invalid work type causes error
  it('create work with invalid duration value causes error', async () => {
    let error = null
    try {
      const workWithInvalidDuration = new WorkModel(invalidWorkDuration)
      await workWithInvalidDuration.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull() // validation error
  })

  // Add material to a work
  it('add (and delete) material to the first work', async () => {
    const work = await WorkModel.findById(firstWorkId)
    expect(work.materials).toHaveLength(0)

    work.materials.push({ material: '5fbe616104cba8484003bc57', number: 1 })
    let savedWork = await work.save()
    expect(savedWork.materials).toHaveLength(1)
    expect(typeof savedWork.materials[0]).toBe('object')
    expect(typeof savedWork.materials[0].material).toBe('object')
    expect(savedWork.materials[0].number).toBe(1)

    savedWork.materials.splice(0, 1)
    savedWork = await savedWork.save()
    expect(savedWork.materials).toHaveLength(0)
  })

  // Get work (first work) by id
  it('get work by id', async () => {
    const work = await WorkModel.findById(firstWorkId)

    const customer = mongoose.Types.ObjectId(work.customer).toHexString()
    const team = mongoose.Types.ObjectId(work.team).toHexString()
    expect(work.type).toBe(validWork.type)
    expect(work.done).toBe(validWork.done)
    expect(work.duration).toBe(validWork.duration)
    expect(typeof work.customer).toBe('object')
    expect(customer).toBe(validWork.customer)
    expect(work.date).toStrictEqual(validWork.date)
    expect(work.endDate).toStrictEqual(validWork.endDate)
    expect(work.cost).toBe(validWork.cost)
    expect(work.problem).toBe(validWork.problem)
    expect(typeof work.team).toBe('object')
    expect(team).toBe(validWork.team)
    expect(work.materials).toHaveLength(validWork.materials.length)
  })

  // Get all works
  it('get all works from db', async () => {
    const works = await WorkModel.find()
    expect(works).toHaveLength(2) // two inserted work
  })

  // Modify first work data
  it('modify work data', async () => {
    const work = await WorkModel.findById(firstWorkId)
    work.type = modifiedWork.type
    work.done = modifiedWork.done
    work.duration = modifiedWork.duration
    work.date = modifiedWork.date
    work.endDate = modifiedWork.endDate
    work.cost = modifiedWork.cost
    work.team = modifiedWork.team
    work.problem = modifiedWork.problem
    work.materials = modifiedWork.materials 
    const savedWork = await work.save()

    const customer = mongoose.Types.ObjectId(savedWork.customer).toHexString()
    const team = mongoose.Types.ObjectId(savedWork.team).toHexString()
    expect(savedWork.type).toBe(modifiedWork.type)
    expect(savedWork.done).toBe(modifiedWork.done)
    expect(savedWork.duration).toBe(modifiedWork.duration)
    expect(typeof savedWork.customer).toBe('object')
    expect(customer).toBe(modifiedWork.customer)
    expect(savedWork.date).toBe(modifiedWork.date)
    expect(savedWork.endDate).toBe(modifiedWork.endDate)
    expect(savedWork.cost).toBe(modifiedWork.cost)
    expect(savedWork.problem).toBe(modifiedWork.problem)
    expect(typeof savedWork.team).toBe('object')
    expect(team).toBe(modifiedWork.team)
    expect(savedWork.materials).toHaveLength(modifiedWork.materials.length)
  })

  // Delete work
  it('delete work', async () => {
    await WorkModel.deleteOne({ _id: firstWorkId })
    const work = await WorkModel.findById(firstWorkId)
    const works = await WorkModel.find()
    expect(work).toBe(null)
    expect(works).toHaveLength(1)
  })
})

/**
 * Complete work model examples.
 */
const validWork = {
  type: 'survey',
  done: false,
  duration: 1,
  customer: '5fbe56f5ede3534a0893a20f',
  date: new Date(),
  endDate: new Date(),
  cost: 15000,
  team: '5fbe5144a284cc3610be63b4',
  problem: 'Test problem',
  materials: []
}

const validWorkReapir = {
  type: 'repair',
  done: false,
  duration: 1,
  customer: '5fbe56f5ede3534a0893a20f',
  date: new Date(),
  endDate: new Date(),
  cost: 15000,
  team: '5fbe5144a284cc3610be63b4',
  problem: 'Test problem',
  materials: []
}

const invalidWorkType = {
  type: 'invalid',
  done: false,
  duration: 1,
  customer: '5fbe56f5ede3534a0893a20f',
  date: new Date(),
  endDate: new Date(),
  cost: 15000,
  team: '5fbe5144a284cc3610be63b4',
  problem: 'Test problem',
  materials: []
}

const invalidWorkCost = {
  type: 'survey',
  done: false,
  duration: 1,
  customer: '5fbe56f5ede3534a0893a20f',
  date: new Date(),
  endDate: new Date(),
  cost: -1,
  team: '5fbe5144a284cc3610be63b4',
  problem: 'Test problem',
  materials: []
}

const invalidWorkDuration = {
  type: 'survey',
  done: false,
  duration: -1,
  customer: '5fbe56f5ede3534a0893a20f',
  date: new Date(),
  endDate: new Date(),
  cost: 15000,
  team: '5fbe5144a284cc3610be63b4',
  problem: 'Test problem',
  materials: []
}

const modifiedWork = {
  type: 'repair',
  done: true,
  duration: 2,
  customer: '5fbe56f5ede3534a0893a20f',
  date: new Date(),
  endDate: new Date(),
  cost: 25000,
  team: '5fbe514aa284cc3610be63b5',
  problem: 'Test problem modified',
  materials: [{ material: '5fbe5dd8ede3534a0893a22a', number: 2 }]
}
