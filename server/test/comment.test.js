const mongoose = require('mongoose')

const dbHandler = require('./db-handler')
const CommentModel = require('../models/comments')

let firstCommentId = null

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => await dbHandler.connect())

/**
 * Remove and close the db and server.
 */
afterAll(async () => await dbHandler.closeDatabase())

/**
 * Comment test suite.
 */
describe('Comment model ', () => {
  it('create & save comment successfully', async () => {
    const comment = new CommentModel(validComment)
    const savedComment = await comment.save()

    const user = mongoose.Types.ObjectId(savedComment.user).toHexString()
    const note = mongoose.Types.ObjectId(savedComment.note).toHexString()
    expect(savedComment._id).toBeDefined()
    expect(typeof savedComment.user).toBe('object')
    expect(user).toEqual(validComment.user)
    expect(typeof savedComment.note).toBe('object')
    expect(note).toBe(validComment.note)
    expect(savedComment.text).toBe(validComment.text)
    expect(savedComment.date).toBe(validComment.date)

    firstCommentId = savedComment._id // use for get comment by id
  })

  // Comment with invalid user id causes error
  it('create comment with invalid user id causes error', async () => {
    let error = null
    try {
      const commentWithInvalidUser = new CommentModel(invalidComment)
      await commentWithInvalidUser.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull() // validation error
  })

  // Comment without reqiured fields causes error
  it('create comment without required fields causes error', async () => {
    let error = null
    try {
      await new CommentModel({}).save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull() // validation error
  })

  // Get comment (first comment) by id
  it('get comment by id', async () => {
    const comment = await CommentModel.findById(firstCommentId)

    const user = mongoose.Types.ObjectId(comment.user).toHexString()
    const note = mongoose.Types.ObjectId(comment.note).toHexString()
    expect(typeof comment.user).toBe('object')
    expect(user).toEqual(validComment.user)
    expect(typeof comment.note).toBe('object')
    expect(note).toBe(validComment.note)
    expect(comment.text).toBe(validComment.text)
    expect(comment.date).toStrictEqual(validComment.date)
  })

  // Get all comments
  it('get all comments from db', async () => {
    // add 2 comment to the db --> should be 3
    await new CommentModel(validComment).save()
    await new CommentModel(validComment).save()

    const comments = await CommentModel.find()
    expect(comments).toHaveLength(3) // three inserted comments
    expect(comments[0].text).toBe(validComment.text)
    expect(comments[1].text).toBe(validComment.text)
    expect(comments[2].text).toBe(validComment.text)
  })

  // Modify first comment data
  it('modify comment data', async () => {
    const comment = await CommentModel.findById(firstCommentId)
    comment.text = modifiedComment.text
    comment.date = modifiedComment.date
    const savedComment = await comment.save()

    const user = mongoose.Types.ObjectId(savedComment.user).toHexString()
    const note = mongoose.Types.ObjectId(savedComment.note).toHexString()
    const commentUser = mongoose.Types.ObjectId(comment.user).toHexString()
    const commentNote = mongoose.Types.ObjectId(comment.note).toHexString()

    expect(user).toBe(commentUser)
    expect(note).toBe(commentNote)
    expect(savedComment.text).toBe(modifiedComment.text)
    expect(savedComment.date).toBe(modifiedComment.date)
  })

  // Delete comment
  it('delete comment', async () => {
    await CommentModel.deleteOne({ _id: firstCommentId })
    const comment = await CommentModel.findById(firstCommentId)
    const comments = await CommentModel.find()
    expect(comment).toBe(null)
    expect(comments).toHaveLength(2)
  })
})

/**
 * Complete comment model examples.
 */
const validComment = {
  user: '5fbe471792cf6928e43c8fb4',
  note: '5fbe6da8ad66e6266cd5e11b',
  text: 'testuser',
  date: new Date('2020-10-13T13:45:52.000+00:00')
}

const invalidComment = {
  user: '1',
  note: '5fbe6da8ad66e6266cd5e11b',
  text: 'testuser',
  date: new Date('2020-10-13T13:45:52.000+00:00')
}

const modifiedComment = {
  text: 'new',
  date: new Date()
}