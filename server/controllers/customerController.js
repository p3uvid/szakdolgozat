const Customer = require("../models/customers")
const Work = require("../models/works")
const Order = require("../models/orders")
const express = require('express')
const router = express.Router()

// Get all customer
router.get('/customers', (req, res) => {
  Customer.find({}, function (error, customers) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Hiba történt az adatok lekérése közben.'
      })
    } else {
      res.send(customers)
    }
  })
})

// Get customer by id
router.get('/customers/:id', (req, res) => {
	Customer.findById(req.params.id, function (error, customer) {
	  if (error) { 
      console.error(error)
      res.status(201).send({
        error,
        message: 'Ügyfél nem található.'
      })
    } else {
      res.send(customer)
    }
	})
})

// Add new customer
router.post('/customers/add', (req, res) => {
  const name = req.body.name
  const address = req.body.address
  const email = req.body.email
  const phone = req.body.phone

  const new_customer = new Customer({
    name, address, email, phone
  })

  new_customer.save(function (error, customer) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Hiba történt az adatok mentése közben.'
      })
    } else {
      res.send({
        success: true,
        message: 'Ügyfél sikeresen mentve az adatbázisba.',
        customer
      })
    }
  })
})

// Delete customer
router.delete('/customers/:id', (req, res) => {
  Customer.remove({ _id: req.params.id }, function(error, customer){
    if (error) {
      res.status(201).send({
        error,
        message: 'Hiba az ügyfél törlése közben.'
      })
    } else {
      let someError = false
      Work.find({ customer: req.params.id}, function (error, works) {
        if (error) {
          someError = true
          res.status(201).send({
            error,
            message: 'Hiba az ügyfél törlés közben.'
          })
        }
        works.forEach(work => {
          work.customer = null

          work.save(function (error) {
            if (error) {
              someError = true
              res.status(201).send({
                error,
                message: 'Hiba történt az adatok módosítása közben.'
              })
            }
          })
        })
      })

      Order.find({ customer: req.params.id}, function (error, orders) {
        if (error) {
          someError = true
          res.status(201).send({
            error,
            message: 'Hiba az ügyfél törlés közben.'
          })
        }
        orders.forEach(order => {
          order.customer = null

          order.save(function (error) {
            if (error) {
              someError = true
              res.status(201).send({
                error,
                message: 'Hiba történt az adatok módosítása közben.'
              })
            }
          })
        })
      })

      if (!someError) {
        res.send({
          success: true,
          message: 'Ügyfél sikeresen törölve az adatbázisból.'
        })
      }
    }
  })
})

// Modify customer
router.put('/customers/:id', (req, res) => {
  Customer.findById(req.params.id, function (error, customer) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Ügyfél nem található.'
      })
    }

    customer.name = req.body.name ? req.body.name : customer.name
    customer.address = req.body.address ? req.body.address : customer.address
    customer.email = req.body.email ? req.body.email : customer.email
    customer.phone = req.body.phone ? req.body.phone : customer.phone

    customer.save(function (error) {
      if (error) {
        res.status(201).send({
          error,
          message: 'Hiba történt az adatok mentése közben.'
        })
      } else {
        res.send({
          success: true,
          message: 'Ügyfél sikeresen mentve az adatbázisba.'
        })
      }
    })
  })
})

module.exports = router
