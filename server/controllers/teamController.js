const Team = require("../models/teams")
const Work = require("../models/works")
const express = require('express')
const router = express.Router()

// Get all teams
router.get('/teams', (req, res) => {
  Team.find({ deleted: { $ne: true }}, function (error, teams) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Hiba az adatok lekérése közben.'
      })
    } else {
      res.send(teams)
    }
  }).populate('members')
})

// Get team by id
router.get('/teams/:id', (req, res) => {
	Team.findById(req.params.id, function (error, team) {
	  if (error) { 
      res.status(201).send({
        error,
        message: 'Brigád nem található.'
      })
    } else {
      res.send(team)
    }
	})
})

// Add new team
router.post('/teams/add', (req, res) => {
  const name = req.body.name
  const members = req.body.members

  const new_team = new Team({
    name, members
  })

  new_team.save(function (error) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Hiba az adatok mentése közben.'
      })
    } else {
      res.send({
        success: true,
        message: 'Brigád sikeresen mentve az adatbázisba.'
      })
    }
  })
})

// Modify team datas
router.put('/teams/:id', (req, res) => {
  Team.findById(req.params.id, function (error, team) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Brigád nem található.'
      })
    }

    team.name = req.body.name ? req.body.name : team.name
    team.members = req.body.members ? req.body.members : team.members

    team.save(function (error) {
      if (error) {
        res.status(201).send({
          error,
          message: 'Hiba az adatok mentése közben.'
        })
      } else {
        res.send({
          success: true,
          message: 'Brigád sikeresen mentve az adatbázisba.'
        })
      }
    })
  })
})

// Assign user to a team
router.put('/teams/adduser/:id', (req, res) => {
  Team.findById(req.params.id, function (error, team) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Brigád nem található.'
      })
    }

    const member = req.body.member
    if (!team.members.includes(member)) {
      team.members.push(member)

      team.save(function (error) {
        if (error) {
          res.status(201).send({
            error,
            message: 'Hiba az adatok mentése közben.'
          })
        } else {
          res.send({
            success: true,
            message: 'Brigád sikeresen mentve az adatbázisba.'
          })
        }
      })
    } else {
      res.send({
        error: true,
        message: 'Ez a felhasználó már tagja a brigádnak!'
      })
    }
  })
})

// Delete user from a team
router.put('/teams/deleteuser/:id', (req, res) => {
  Team.findById(req.params.id, function (error, team) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Brigád nem található.'
      })
    }

    const memberId = team.members.indexOf(req.body.member)
    if (memberId >= 0) {
      team.members.splice(memberId, 1)
    }

    team.save(function (error) {
      if (error) {
        res.status(201).send({
          error,
          message: 'Hiba az adatok mentése közben.'
        })
      } else {
        res.send({
          success: true,
          message: 'Brigád sikeresen módosítva lett.'
        })
      }
    })
  })
})

// Delete team
router.delete('/teams/:id', (req, res) => {
  Team.findById(req.params.id, function(error, team) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Hiba az adatok törlése közben.'
      })
    } else {
      if (!team.deleted) {
        team.deleted = true
        team.save(function (error) {
          if (error) {
            res.status(201).send({
              error,
              message: 'Hiba történt az adatok törlése közben.'
            })
          } else {
            res.send({
              success: true,
              message: 'Brigád sikeresen törölve az adatbázisból.'
            })
          }
        })
      } else {
        res.status(201).send({
          error: true,
          message: 'Hiba történt az adatok törlése közben.'
        })
      }
    }
  })
})

module.exports = router
