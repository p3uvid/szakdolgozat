const Order = require("../models/orders")
const express = require('express')
const router = express.Router()

// Get all orders
router.get('/orders', (req, res) => {
  Order.find({ customer: { $ne: null }, deleted: { $ne: true }}, function (error, orders) {
    if (error) { 
      res.status(201).send({
        error,
        message: 'Hiba történt az adatok lekérése közben.'
      })
    } else {
      res.send(orders)
    }
  }).populate('customer').populate('works').populate({ path: 'works', populate: { path: 'team' }})
})

// Get order by id
router.get('/orders/:id', (req, res) => {
	Order.findById(req.params.id, function (error, order) {
	  if (error) { 
      res.status(201).send({
        error,
        message: 'A megrendelés nem található.'
      })
    } else {
      res.send(order)
    }
	})
})

// Add new order
router.post('/orders/add', (req, res) => {
  const customer = req.body.customer
  const status = req.body.status
  const works = req.body.works
  const note = req.body.note

  if (status && (status !== 'survey' && status !== 'waitingForRepair' && status !== 'repair' && status !== 'done')) {
    res.send({
      error: true,
      message: 'Érvénytelen státusz!'
    })
  } else {
    const new_order = new Order({
      customer, status, works, note
    })
  
    new_order.save(function (error) {
      if (error) {
        res.status(201).send({
          error,
          message: 'Hiba az adatok mentése közben.'
        })
      } else {
        res.send({
          success: true,
          message: 'Megrendelés sikeresen mentve az adatbázisba.'
        })
      }
    })
  }
})

router.put('/orders/addwork/:id', (req, res) => {
  Order.findById(req.params.id, function (error, order) {
    if (error) { 
      res.status(201).send({
        error,
        message: 'Megrendelés nem található.'
      })
    }

    order.works.push(req.body.work)

    order.save(function (error) {
      if (error) {
        res.status(201).send({
          error,
          message: 'Hiba az adatok mentése közben.'
        })
      } else {
        res.send({
          success: true,
          message: 'Megrendelés sikeresen módosítva lett.'
        })
      }
    })
  })
})

// Delete work from the order
router.put('/orders/deletework/:id', (req, res) => {
  Order.findById(req.params.id, function (error, order) {
    if (error) { 
      res.status(201).send({
        error,
        message: 'Megrendelés nem található.'
      })
    }

    const workId = order.works.indexOf(req.body.work)
    if (workId >= 0) {
      order.works.splice(workId, 1)
    }

    order.save(function (error) {
      if (error) {
        res.status(201).send({
          error,
          message: 'Hiba az adatok mentése közben.'
        })
      } else {
        res.send({
          success: true,
          message: 'Megrendelés sikeresen módosítva lett.'
        })
      }
    })
  })
})


// Modify order
router.put('/orders/:id', (req, res) => {
  Order.findById(req.params.id, function (error, order) {
    if (error) { 
      res.status(201).send({
        error,
        message: 'Megrendelés nem található.'
      })
    }

    order.customer = req.body.customer ? req.body.customer : order.customer

    const status = req.body.status
    if (status && (status === 'survey' || status === 'waitingForRepair' || status === 'repair' || status === 'done')) {
      order.status = status
    } else {
      res.send({
        error: true,
        message: 'Érvénytelen státusz!'
      })
    }

    order.works = req.body.works ? req.body.works : order.works
    order.note = req.body.note ? req.body.note : order.note

    order.save(function (error) {
      if (error) {
        res.status(201).send({
          error,
          message: 'Hiba az adatok mentése közben.'
        })
      } else {
        res.send({
          success: true,
          message: 'Megrendelés sikeresen módosítva lett.'
        })
      }
    })
  })
})

// Delete order
router.delete('/orders/:id', (req, res) => {
  Order.findById(req.params.id, function(error, order) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Hiba az adatok törlése közben.'
      })
    } else {
      if (!order.deleted) {
        order.deleted = true
        order.save(function (error) {
          if (error) {
            res.status(201).send({
              error,
              message: 'Hiba történt az adatok törlése közben.'
            })
          } else {
            res.send({
              success: true,
              message: 'Megrendelés sikeresen törölve lett az adatbázisból.'
            })
          }
        })
      } else {
        res.status(201).send({
          error: true,
          message: 'Hiba történt az adatok törlése közben.'
        })
      }
    }
  })
})

module.exports = router
