const Work = require("../models/works")
const Material = require("../models/materials")
const express = require('express')
const router = express.Router()

// Get all works
router.get('/works', (req, res) => {
  Work.find({ customer: { $ne: null }, deleted: { $ne: true } }, function (error, works) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Hiba az adatok lekérése közben.'
      })
    } else {
      res.send(works)
    }
  }).populate('team').populate('customer').populate({ path: 'materials', populate: { path: 'material' }})
})

// Get survey works
router.get('/works/surveys', (req, res) => {
  Work.find({ type: "survey", customer: { $ne: null }, deleted: { $ne: true } }, function (error, surveys) {
	  if (error) {
      res.status(201).send({
        error,
        message: 'Munka nem található.'
      })
    } else {
      res.send(surveys)
    }
	}).populate('team').populate('customer').populate({ path: 'materials', populate: { path: 'material' }})
})

// Get repair works
router.get('/works/repairs', (req, res) => {
  Work.find({ type: "repair", customer: { $ne: null }, deleted: { $ne: true } }, function (error, repairs) {
	  if (error) {
      res.status(201).send({
        error,
        message: 'Munka nem található.'
      })
    } else {
      res.send(repairs)
    }
	}).populate('team').populate('customer').populate({ path: 'materials', populate: { path: 'material' }})
})

// Get work by id
router.get('/works/:id', (req, res) => {
	Work.findById(req.params.id, function (error, work) {
	  if (error) {
      res.status(201).send({
        error,
        message: 'Munka nem található.'
      })
    } else {
      res.send(work)
    }
	}).populate('customer').populate('team').populate({ path: 'materials', populate: { path: 'material' }})
})

// Add new work
router.post('/works/add', (req, res) => {
  const customer = req.body.customer
  const type = req.body.type
  const date = req.body.date
  const endDate = req.body.endDate
  const duration = req.body.duration
  const cost = req.body.cost
  const team = Object.keys(req.body.team).length !== 0 ? req.body.team : null
  const problem = req.body.problem
  const materials = req.body.materials
  const note = req.body.note

  if (type && (type !== 'survey' && type !== 'repair')) {
    res.send({
      error: true,
      message: 'Érvénytelen típus!'
    })
  } else {
    const new_work = new Work({
      customer, type, date, endDate, duration, cost, team, problem, materials, note
    })
  
    new_work.save(function (error, work) {
      if (error) {
        res.status(201).send({
          error,
          message: 'Hiba az adatok mentése közben.'
        })
      } else {
        res.send({
          success: true,
          work,
          message: 'Munka sikeresen mentve az adatbázisba.'
        })
      }
    })
  }
})

// Modify work
router.put('/works/:id', (req, res) => {
  Work.findById(req.params.id, function (error, work) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Munka nem található.'
      })
    }

    work.customer = req.body.customer ? req.body.customer : work.customer

    const type = req.body.type
    if (type) {
      if (type === 'survey' || type === 'repair' ) {
        work.type = type
      } else {
        res.send({
          error: true,
          message: 'Érvénytelen típus!'
        })
        return
      }
    }

    // Set work status done -> subtract the number from the material storage
    if (req.body.done && !work.done) {
      work.done = req.body.done
      const materials = work.materials
      materials.forEach(material => {
        Material.findById(material.material, function (error, dbMaterial) {
          if (error) {
            res.status(201).send({
              error,
              message: 'Hiba történt az adatok módosítása közben.'
            })
          }
          dbMaterial.number -= material.number

          dbMaterial.save(function (error) {
            if (error) {
              res.status(201).send({
                error,
                message: 'Hiba történt az adatok módosítása közben.'
              })
            }
          })
        })
      })
    } else if (work.done) {
      work.done = req.body.done
      const materials = work.materials
      materials.forEach(material => {
        Material.findById(material.material, function (error, dbMaterial) {
          if (error) {
            res.status(201).send({
              error,
              message: 'Hiba történt az adatok módosítása közben.'
            })
          }
          dbMaterial.number += material.number

          dbMaterial.save(function (error) {
            if (error) {
              res.status(201).send({
                error,
                message: 'Hiba történt az adatok módosítása közben.'
              })
            }
          })
        })
      })
    }

    work.date = req.body.date ? req.body.date : work.date
    work.endDate = req.body.endDate ? req.body.endDate : work.endDate
    work.duration = req.body.duration ? req.body.duration : work.duration
    work.cost = req.body.cost ? req.body.cost : work.cost
    work.team = req.body.team ? req.body.team : work.team
    work.problem = req.body.problem ? req.body.problem : work.problem
    work.materials = req.body.materials ? req.body.materials : work.materials
    work.note = req.body.note ? req.body.note : work.note

    work.save(function (error) {
      if (error) {
        res.status(201).send({
          error,
          message: 'Hiba az adatok mentése közben.'
        })
      } else {
        res.send({
          success: true,
          work,
          message: 'Munka sikeresen mentve az adatbázisba.'
        })
      }
    })
  })
})

// Delete work
router.delete('/works/:id', (req, res) => {
  Work.findById(req.params.id, function(error, work) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Hiba az adatok törlése közben.'
      })
    } else {
      if (!work.deleted) {
        work.deleted = true
        work.save(function (error) {
          if (error) {
            res.status(201).send({
              error,
              message: 'Hiba történt az adatok törlése közben.'
            })
          } else {
            res.send({
              success: true,
              message: 'Munka sikeresen törölve az adatbázisból.'
            })
          }
        })
      } else {
        res.status(201).send({
          error: true,
          message: 'Hiba történt az adatok törlése közben.'
        })
      }
    }
  })
})

module.exports = router
