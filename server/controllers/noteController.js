const Note = require("../models/notes")
const Comment = require("../models/comments")
const express = require('express')
const router = express.Router()

// Get all note
router.get('/notes', (req, res) => {
  Note.find({}, function (error, notes) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Hiba történt az adatok lekérése közben.'
      })
    } else {
      res.send(notes)
    }
  }).populate('user')
})

// Get note by id
router.get('/notes/:id', (req, res) => {
	Note.findById(req.params.id, function (error, note) {
	  if (error) {
      res.status(201).send({
        error,
        message: 'Bejegyzés nem található.'
      })
    } else {
      res.send(note)
    }
	})
})

// Add new note
router.post('/notes/add', (req, res) => {
  const user = req.body.user
  const title = req.body.title
  const text = req.body.text
  const date = req.body.date

  const new_note = new Note({
    user, title, text, date
  })

  new_note.save(function (error, note) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Hiba történt az adatok mentése közben.'
      })
    } else {
      res.send({
        success: true,
        message: 'Bejegyzés sikeresen mentve az adatbázisba.',
        note
      })
    }
  })
})

// Delete note
router.delete('/notes/:id', (req, res) => {
  Note.remove({ _id: req.params.id }, function(error, note){
    if (error) {
      res.status(201).send({
        error,
        message: 'Hiba a bejegyzés törlése közben.'
      })
    } else {
      let someError = false
      Comment.find({ note: req.params.id }, function (error, comments) {
        if (error) {
          res.status(201).send({
            error,
            message: 'Hiba az adatok törlése közben.'
          })
        } else {
          comments.forEach(comment => {
            comment.note = null
  
            comment.save(function (error) {
              if (error) {
                someError = true
                res.status(201).send({
                  error,
                  message: 'Hiba történt az adatok törlése közben.'
                })
              }
            })
          })

          if (!someError) {
            res.send({
              success: true,
              message: 'Bejegyzés sikeresen törölve az adatbázisból.'
            })
          }
        }
      })
    }
  })
})

// Get comments of a note
router.get('/notes/:id/comments', (req, res) => {
  Comment.find({ note: req.params.id}, function(error, comments) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Hiba történt az adatok lekérése közben.'
      })
    } else {
      res.send(comments)
    }
  }).populate('user')
})

// Add new comment
router.post('/notes/:id/addComment', (req, res) => {
  const user = req.body.user
  const note = req.body.note
  const text = req.body.text
  const date = req.body.date

  const new_comment = new Comment({
    user, note, text, date
  })

  new_comment.save(function (error, comment) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Hiba történt az adatok mentése közben.'
      })
    } else {
      res.send({
        success: true,
        message: 'Komment sikeresen mentve a bejegyzéshez.',
        note
      })
    }
  })
})


module.exports = router
