const User = require('../models/users')
const Team = require('../models/teams')
const express = require('express')
const { some } = require('bluebird')
const router = express.Router()

// Get all users
router.get('/users', (req, res) => {
  User.find({ deleted: { $ne: true }}, function (error, users) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Hiba az adatok lekérése közben.'
    })
    } else {
      res.send(users)
    }
  })
})

// Get user by id
router.get('/users/:id', (req, res) => {
	User.findById(req.params.id, function (error, user) {
	  if (error) {
      res.status(201).send({
        error,
        message: 'Felhasználó nem található.'
      })
    } else {
      res.send(user)
    }
	})
})

// Post user (registration - add new user)
router.post('/users', (req, res) => {
  const name = req.body.name
  const email = req.body.email
  const username = req.body.username
  const password = req.body.password
  const role = req.body.role

  // Existing email check
  User.find({}, function (error, users) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Hiba az adatok lekérése közben.'
      })
    } else {
      let emails = users.map((user) => { return user.email })
      if (emails.includes(email)) {
        res.status(201).send({
          error: true,
          message: 'Ez az e-mail cím már foglalt!'
        })
      } else {
        const new_user = new User({
          name, email, username, password, role
        })
      
        new_user.save(function (error) {
          if (error) {
            res.status(201).send({
              error,
              message: 'Hiba az adatok mentése közben.'
            })
          } else {
            res.send({
              success: true,
              message: 'Felhasználó sikeresen mentve az adatbázisba.'
            })
          }
        })
      }
    }
  })
})

// Modify user
router.put('/users/:id', (req, res) => {
  User.findById(req.params.id, function (error, user) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Felhasználó nem található.'
      })
    }

    let users = User.find({}, function (error, users) {
      if (error) {
        res.status(201).send({
          error,
          message: 'Hiba az adatok módosítása közben.'
        })
      } else {
        let emails = users.map((user) => { return user.email })
        if (user.email !== req.body.email && emails.includes(req.body.email)) {
          res.status(201).send({
            error: true,
            message: 'Ez az e-mail cím már foglalt!'
          })
        } else {
          user.name = req.body.name ? req.body.name : user.name
          user.email = req.body.email ? req.body.email : user.email
          user.username = req.body.username ? req.body.username : user.username
          user.password = req.body.password ? req.body.password : user.password
          user.role = req.body.role ? req.body.role : user.role

          user.save(function (error) {
            if (error) {
              res.status(201).send({
                error,
                message: 'Hiba az adatok mentése közben.'
              })
            } else {
              res.send({
                success: true,
                message: 'Felhasználó sikeresen mentve az adatbázisba.'
              })
            }
          })
        }
      }
    })
  })
})

// Modify password
router.put('/users/password/:id', (req, res) => {
  User.findById(req.params.id, function (error, user) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Felhasználó nem található.'
      })
    } else {
      const mongoUser = new User(user)
      const isPasswordValid = mongoUser.comparePassword(req.body.oldPw)
      if (!isPasswordValid) {
        return res.send({
          error: true,
          message: 'Helytelen a régi jelszó.'
        })
      } else {
        user.password = req.body.newPw
        user.save(function (error) {
          if (error) {
            res.status(201).send({
              error,
              message: 'Hiba az adatok mentése közben.'
            })
          } else {
            res.send({
              success: true,
              message: 'Jelszó sikeresen módosítva.'
            })
          }
        })
      }
    }
  })
})

// Restore password by admin user
router.put('/users/restorePassword/:id', (req, res) => {
  User.findById(req.params.id, function (error, user) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Felhasználó nem található.'
      })
    } else {
      User.findById(req.body.admin, function (error, admin) {
        if (error) {
          res.status(201).send({
            error,
            message: 'Felhasználó nem található.'
          })
        } else if (admin.role !== 'admin' && admin.role !== 'office') {
          res.status(201).send({
            error: true,
            message: 'Nincs jogosultság a művelet végrehajtásához.'
          })
        } else {
          user.password = req.body.password
          user.save(function (error) {
            if (error) {
              res.status(201).send({
                error,
                message: 'Hiba az adatok mentése közben.'
              })
            } else {
              res.send({
                success: true,
                message: 'Jelszó sikeresen módosítva.'
              })
            }
          })
        }
      })
    }
  })
})

// Delete user
router.delete('/users/:id', (req, res) => {
  let someError = false
  User.findById(req.params.id, function(error, user) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Hiba az adatok törlése közben.'
      })
    } else {
      if (!user.deleted) {
        user.deleted = true

        const memberId = req.params.id
        Team.find({}, function (error, teams) {
          if (error) {
            someError = true
            res.status(201).send({
              error,
              message: 'Hiba az adatok lekérése közben.'
            })
          } else {
            teams.forEach(team => {
              if(team.members.includes(memberId)) {
                const mId = team.members.indexOf(memberId)
                team.members.splice(mId, 1)

                team.save(function (error) {
                  if (error) {
                    someError = true
                    res.status(201).send({
                      error,
                      message: 'Hiba az adatok mentése közben.'
                    })
                  }
                })
              }
            })
          }
        })
        if (!someError) {
          user.save(function (error) {
            if (error) {
              res.status(201).send({
                error,
                message: 'Hiba történt az adatok törlése közben.'
              })
            } else {
              res.send({
                success: true,
                message: 'Felhasználó sikeresen törölve az adatbázisból.'
              })
            }
          })
        }
      } else {
        res.status(201).send({
          error: true,
          message: 'Hiba történt az adatok törlése közben.'
        })
      }
    }
  })
})

module.exports = router