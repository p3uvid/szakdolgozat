const jwt = require('jsonwebtoken')

const User = require("../models/users")
const Team = require("../models/teams")
const express = require('express')
const router = express.Router()

function jwtSignUser(user) {
  user = user.toJSON()
  const ONE_WEEK = 60 * 60 * 24 * 7
  return jwt.sign(user, 'secret', {
    expiresIn: ONE_WEEK
  })
}

router.post('/login', (req, res) => {
  try {
    const { email, password } = req.body
    User.findOne({ email: email }, function (error, user) {
      if (error) {
        res.status(201).send({
          error,
          message: 'Hiba történt bejelentkezés közben.'
        })
      } else {
        if (!user) {
          return res.status(201).send({
            error,
            message: 'Helytelen e-mail cím.'
          })
        }
        if (user.deleted) {
          return res.status(201).send({
            error,
            message: 'A belépés nem engedélyezett.'
          })
        }

        const mongoUser = new User(user)
        
        const isPasswordValid = mongoUser.comparePassword(password)
        if (!isPasswordValid) {
          return res.status(201).send({
            error,
            message: 'Helytelen jelszó.'
          })
        }

        // Seraching for the user's team
        let userTeams = []
        Team.find({}, function (error, teams) {
          if (error) {
            res.status(201).send({
              error,
              message: 'Hiba az adatok lekérése közben.'
            })
          } else {
            teams.forEach(team => {
              if(team.members.includes(user._id)) {
                userTeams.push(team._id)
              }
            })

            res.send({
              user: user,
              token: jwtSignUser(user),
              userTeams: userTeams
            })
          }
        })
      }
    })

  } catch (error) {
    res.status(201).send({
      error,
      message: 'Hiba történt a bejelentkezés alatt.'
    })
  }
})

module.exports = router
