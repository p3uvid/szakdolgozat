const Material = require("../models/materials")
const express = require('express')
const Work = require("../models/works")
const router = express.Router()

// Get all materials
router.get('/materials', (req, res) => {
  Material.find({}, function (error, materials) {
    if (error) {
      res.status(201).send({ 
        error,
        message: 'Hiba történt az adatok lekérése közben.' 
      })
    } else {
      res.send(materials)
    }
  })
})

// Get material by id
router.get('/materials/:id', (req, res) => {
	Material.findById(req.params.id, function (error, material) {
	  if (error) {
      res.status(201).send({
        error,
        message: 'Ez az anyag nem található az anyagnyilvántartóban.'
      })
    } else {
      res.send(material)
    }
	})
})

// Modify material status
router.put('/materials/status/:id', (req, res) => {
  Material.findById(req.params.id, function (error, material) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Ez az anyag nem található az anyagnyilvántartóban.'
      })
    }

    const status = req.body.status
    if (status && (status === 'inStock' || status === 'order' || status === 'few')) {
      material.status = status
    } else {
      res.send({
        error: true,
        message: 'Érvénytelen státusz!'
      })
    }

    material.save(function (error) {
      if (error) {
        res.status(201).send({
          error,
          message: 'Hiba történt mentés közben!'
        })
      } else {
        res.send({
          success: true,
          message: 'Az anyag sikeresen mentve az anyagnyilvántartóba.'
        })
      }
    })
  })
})

// Modify material datas
router.put('/materials/:id', (req, res) => {
  Material.findById(req.params.id, function (error, material) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Ez az anyag nem található az anyagnyilvántartóban.'
      })
    }

    material.name = req.body.name ? req.body.name : material.name
    material.price = req.body.price ? req.body.price : material.price
    material.number = req.body.number ? req.body.number : material.number
    material.status = req.body.status ? req.body.status : material.status

    material.save(function (error) {
      if (error) {
        res.status(201).send({
          error,
          message: 'Hiba történt mentés közben!'
        })
      } else {
        res.send({
          success: true,
          message: 'Az anyag sikeresen módosítva lett az anyagnyilvántartóban.'
        })
      }
    })
  })
})

// Add new material
router.post('/materials/add', (req, res) => {
  const name = req.body.name
  const price = req.body.price
  const number = req.body.number
  const status = req.body.status

  const new_material = new Material({
    name, price, number, status
  })

  new_material.save(function (error) {
    if (error) {
      res.status(201).send({
        error,
        message: 'Hiba történt mentés közben!'
      })
    } else {
      res.send({
        success: true,
        message: 'Az anyag sikeresen mentve az anyagnyilvántartóba.'
      })
    }
  })
})

// Delete material
router.delete('/materials/:id', (req, res) => {
  Material.remove({ _id: req.params.id }, function(error, material){
    if (error) {
      res.status(201).send({
        error,
        message: 'Hiba történt törlés közben!'
      })
    } else {
      Work.find({}, function (error, works) {
        if (error) {
          res.status(201).send({
            error,
            message: 'Hiba történt törlés közben!'
          })
        } else {
          let someError = false
          works.forEach(work => {
            work.materials.forEach((material, index) => {
              if (material.material == req.params.id) {
                work.materials.splice(index, 1)

                work.save(function (error) {
                  if (error) {
                    someError = true
                    res.status(201).send({
                      error,
                      message: 'Hiba történt az adatok módosítása közben.'
                    })
                  }
                })
              }
            })
          })

          if (!someError) {
            res.send({
              success: true,
              message: 'Az anyag sikeresen törölve lett az anyagnyilvántartóból.'
            })
          }
        }
      })
    }
  })
})

module.exports = router
