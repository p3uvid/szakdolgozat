const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')

const app = express()
app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors())

// Database connection
const mongoose = require('mongoose')
// mongoose.connect('mongodb://localhost:27017/ablakszerviz');
try {
  mongoose.connect('mongodb+srv://admin:admin@cluster0.zovba.mongodb.net/ablakszerviz', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  })
  const db = mongoose.connection;
  db.on("error", console.error.bind(console, "connection error"))
  db.once("open", function (callback) {
    console.log("Connection Succeeded")
  })
} catch (err) {
  console.error('Failed to connect: ', err)
}

const routes = ['auth', 'customer', 'material', 'order', 'team', 'user', 'work', 'note']
routes.map((route) => {
  const contoller = require(`../controllers/${route}Controller`)
  app.use(contoller)
})

app.listen(process.env.PORT || 8081)