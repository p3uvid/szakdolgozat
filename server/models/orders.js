const mongoose = require("mongoose")
const Schema = mongoose.Schema

const OrderSchema = new Schema({
  customer: { type: mongoose.Schema.Types.ObjectId, ref: 'Customer', required: true },
  status: {
    type: String,
    enum: [ 'survey', 'waitingForRepair', 'repair', 'done' ],
    default: 'survey'
  },
  works: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Work',
    default: []
  }],
  note: String,
  deleted: { type: Boolean, default: false }
})

const Order = mongoose.model("Order", OrderSchema)
module.exports = Order