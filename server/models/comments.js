const mongoose = require("mongoose")
const Schema = mongoose.Schema

const CommentSchema = new Schema({
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  note: { type: mongoose.Schema.Types.ObjectId, ref: 'Note', required: true },
  text: { type: String, required: true },
  date: { type: Date, required: true }
})

const Comment = mongoose.model("Comment", CommentSchema)
module.exports = Comment