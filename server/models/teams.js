const mongoose = require("mongoose")
const Schema = mongoose.Schema

const TeamSchema = new Schema({
  name: { type: String, required: true },
  members: [{
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User'
  }],
  deleted: { type: Boolean, default: false }
})

const Team = mongoose.model("Team", TeamSchema)
module.exports = Team