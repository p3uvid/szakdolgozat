const mongoose = require("mongoose")
const Schema = mongoose.Schema

const NoteSchema = new Schema({
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  title: String,
  text: { type: String, required: true },
  date: { type: Date, required: true }
})

const Note = mongoose.model("Note", NoteSchema)
module.exports = Note