const mongoose = require("mongoose")
const Schema = mongoose.Schema

const MaterialSchema = new Schema({
  name: { type: String, required: true },
  price: Number,
  number: {
    type: Number,
    min: 0,
    default: 0
  },
  status: {
    type: String,
    enum: [ 'inStock', 'few', 'order' ],
    default: 'few'
  }
})

const Material = mongoose.model("Material", MaterialSchema)
module.exports = Material