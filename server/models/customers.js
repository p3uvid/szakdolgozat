const mongoose = require("mongoose")
const Schema = mongoose.Schema

const CustomerSchema = new Schema({
  name: { type: String, required: true },
  address: { type: String, required: true },
  email: {
    type: String,
    required: true,
    match: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g,
    unique: true
  },
  phone: { type: String, required: true }
})

const Customer = mongoose.model("Customer", CustomerSchema)
module.exports = Customer