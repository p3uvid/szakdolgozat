const mongoose = require("mongoose")
const Schema = mongoose.Schema

const UserSchema = new Schema({
  name: { type: String, required: true },
  username: { type: String, required: true },
  email: {
    type: String,
    required: true,
    match: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g,
    unique: true
  },
  password: { type: String, required: true },
  role: {
    type: String,
    enum: [ 'admin', 'office', 'teamleader', 'user' ],
    default: 'user'
  },
  deleted: { type: Boolean, default: false }
})

const Promise = require('bluebird')
const bcrypt = Promise.promisifyAll(require('bcrypt-nodejs'))
UserSchema.methods.comparePassword = function (password) {
  return bcrypt.compareSync(password, this.password)
}

UserSchema.methods.hashPassword = function (pw) {
  const saltRounds = 10
  return bcrypt.genSalt(saltRounds, (err, salt) => {
    bcrypt.hash(pw, salt, null, (err, hash) => {
      this.password = hash
    })
  })
}

UserSchema.pre('save', function(next) {
  try {
    if(!this.isModified("password")) {
      return next()
    }
    const saltRounds = 10
    bcrypt.genSalt(saltRounds, (err, salt) => {
      bcrypt.hash(this.password, salt, null, (err, hash) => {
        this.password = hash
      })
    })
    next()
  } catch (err) {
    next(err)
  }
})

const User = mongoose.model("User", UserSchema)
module.exports = User