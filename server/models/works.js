const mongoose = require("mongoose")
const Schema = mongoose.Schema

const WorkSchema = new Schema({
  customer: { type: mongoose.Schema.Types.ObjectId, ref: 'Customer', required: true },
  type: {
    type: String,
    enum: [ 'survey', 'repair' ],
    default: 'survey'
  },
  done: {
    type: Boolean,
    default: false
  },
  date: Date.UTC(),
  endDate: Date.UTC(),
  duration: {
    type: Number,
    min: 0.5,
    default: 0.5
  },
  cost: { type: Number, required: true, min: 0 },
  team: { type: mongoose.Schema.Types.ObjectId, ref: 'Team' },
  problem: { type: String, required: true },
  materials: [{
    number: Number,
    material: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Material'
    },
  }],
  note: String,
  deleted: { type: Boolean, default: false }
})

const Work = mongoose.model("Work", WorkSchema)
module.exports = Work